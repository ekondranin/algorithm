#include <iostream>

using namespace std;

const int MAXN = 1000000;

int t[4 * MAXN];

void build (int a[], int v, int tl, int tr) {
    if (tl == tr) {
        t[v] = a[tl];
    } else {
        int tm = (tl + tr) / 2;
        build (a, v * 2 + 1, tl, tm);
        build (a, v * 2 + 2, tm + 1, tr);
        t[v] = t[v * 2 + 1] + t[v * 2 + 2];
    }
}

int sum (int v, int tl, int tr, int l, int r) {
    if (l > r) {
        return 0;
    }
    if (l == tl && r == tr) {
        return t[v];
    }
    int tm = (tl + tr) / 2;
    return sum (v * 2 + 1, tl, tm, l, min(r, tm))
+ sum (v * 2 + 2, tm + 1, tr, max(l, tm + 1), r);
}

void update (int v, int tl, int tr, int pos, int new_val) {
    if (tl == tr) {
        t[v] = new_val;
    } else {
        int tm = (tl + tr) / 2;
        if (pos <= tm) {
            update (v * 2 + 1, tl, tm, pos, new_val);
        } else {
            update (v * 2 + 2, tm + 1, tr, pos, new_val);
        }
        t[v] = t[v * 2 + 1] + t[v * 2 + 2];
    }
}

template <class T> class SegmentTree
{
    int maxn = 1000000;
    vector <T> t;
    void build(const vector <T> &a, int v, int tl, int tr) {
        if (tl == tr) {
            t[v] = a[tl];
        } else {
            int tm = (tl + tr) / 2;
            build (a, v * 2 + 1, tl, tm);
            build (a, v * 2 + 2, tm + 1, tr);
            t[v] = t[v * 2 + 1] + t[v * 2 + 2];
        }
    }
    T sum(int v, int tl, int tr, int l, int r) {
        if (l > r) {
            return 0;
        }
        if (l == tl && r == tr) {
            return t[v];
        }
        int tm = (tl + tr) / 2;
        return sum (v * 2 + 1, tl, tm, l, min(r, tm))
+ sum (v * 2 + 2, tm + 1, tr, max(l, tm + 1), r);
    }
    void update(int v, int tl, int tr, int pos, T new_val) {
        if (tl == tr) {
            t[v] += new_val;
        } else {
            int tm = (tl + tr) / 2;
            if (pos <= tm) {
                update (v * 2 + 1, tl, tm, pos, new_val);
            } else {
                update (v * 2 + 2, tm + 1, tr, pos, new_val);
            }
            t[v] = t[v * 2 + 1] + t[v * 2 + 2];
        }
    }
public:
    SegmentTree (int n)
    {
        maxn = n;
        t.resize(4 * maxn);
    }
    SegmentTree (const vector <T> &a) {
        maxn = a.size();
        t.resize(4 * maxn);
        build(a, 0, 0, maxn - 1);
    }
    void update(int pos, T add)
    {
        update(0, 0, maxn - 1, pos, add);
    }
    T sum(int l, int r)
    {
        return sum(0, 0, maxn - 1, l, r);
    }
};

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
