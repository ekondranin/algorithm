#include <iostream>

using namespace std;

const int MAXN = 1000000;

int t[4 * MAXN];
int idx[4 * MAXN];

PII sum (int v, int tl, int tr, int l, int r) {
    if (l > r) {
        return {-1, -1};
    }
    if (l == tl && r == tr) {
        return {t[v], idx[v]};
    }
    int tm = (tl + tr) / 2;
    PII a = sum (v * 2 + 1, tl, tm, l, min(r, tm));
    PII b = sum (v * 2 + 2, tm + 1, tr, max(l, tm + 1), r);
    if (a.first > b.first) {
        return a;
    }
    return b;
}

void update (int v, int tl, int tr, int pos, PII new_val) {
    if (tl == tr) {
        t[v] = new_val.first;
        idx[v] = new_val.second;
    } else {
        int tm = (tl + tr) / 2;
        if (pos <= tm) {
            update (v * 2 + 1, tl, tm, pos, new_val);
        } else {
            update (v * 2 + 2, tm + 1, tr, pos, new_val);
        }
        if (t[v * 2 + 1] > t[v * 2 + 2]) {
            t[v] = t[v * 2 + 1];
            idx[v] = idx[v * 2 + 1];
        } else {
            t[v] = t[v * 2 + 2];
            idx[v] = idx[v * 2 + 2];
        }
    }
}

template <class T> class SegmentTree//?????
{
    int maxn = 1000000;
    vector <T> t;
    vector <T> idx;
    void build(const vector <T> &a, int v, int tl, int tr) {
        if (tl == tr) {
            t[v] = a[tl];
            idx[v] = tl;
        } else {
            int tm = (tl + tr) / 2;
            build (a, v * 2 + 1, tl, tm);
            build (a, v * 2 + 2, tm + 1, tr);
            if (t[v * 2 + 1] < t[v * 2 + 2]) {
                t[v] = t[v * 2 + 1];
                idx[v] = idx[v * 2 + 1];
            } else {
                t[v] = t[v * 2 + 2];
                idx[v] = idx[v * 2 + 2];
            }
        }
    }
    pair <T, T> sum (int v, int tl, int tr, int l, int r) {
        if (l > r) {
            return {-1, -1};
        }
        if (l == tl && r == tr) {
            return {t[v], idx[v]};
        }
        int tm = (tl + tr) / 2;
        pair <T, T> a = sum (v * 2 + 1, tl, tm, l, min(r, tm));
        pair <T, T> b = sum (v * 2 + 2, tm + 1, tr, max(l, tm + 1), r);
        if (a.first < b.first) {
            return a;
        }
        return b;
    }
    void update (int v, int tl, int tr, int pos, pair <T, T> new_val) {
        if (tl == tr) {
            t[v] = new_val.first;
            idx[v] = new_val.second;
        } else {
            int tm = (tl + tr) / 2;
            if (pos <= tm) {
                update (v * 2 + 1, tl, tm, pos, new_val);
            } else {
                update (v * 2 + 2, tm + 1, tr, pos, new_val);
            }
            if (t[v * 2 + 1] < t[v * 2 + 2]) {
                t[v] = t[v * 2 + 1];
                idx[v] = idx[v * 2 + 1];
            } else {
                t[v] = t[v * 2 + 2];
                idx[v] = idx[v * 2 + 2];
            }
        }
    }
public:
    SegmentTree () {}
    SegmentTree (int n)
    {
        maxn = n;
        t.resize(4 * maxn);
        idx.resize(4 * maxn);
    }
    SegmentTree (const vector <T> &a) {
        maxn = a.size();
        t.resize(4 * maxn);
        idx.resize(4 * maxn);
        build(a, 0, 0, maxn - 1);
    }
    void update(int pos, T add)
    {
        update(0, 0, maxn - 1, pos, add);
    }
    T sum(int l, int r)
    {
        return sum(0, 0, maxn - 1, l, r);
    }
};


int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
