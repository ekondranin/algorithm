#include <iostream>
#include <vector>

using namespace std;

vector <int> parent;
vector <int> r;

void make_set(int v) {
	parent[v] = v;
	r[v] = 1;
}

int find_set(int t)
{
    if (t == parent[t]) {
        return t;
    }
    return parent[t] = find_set(parent[t]);
}

void union_sets(int a, int b)
{
    a = find_set(a);
    b = find_set(b);
    if (a != b) {
        if (r[a] < r[b]) {
            swap(a, b);
        }
        parent[b] = a;
        if (r[a] == r[b]) {
            ++r[a];
        }
    }
}

int init(int n)
{
    r.resize(n);
    parent.resize(n);
    FOR (i, 0, n) {
        make_set(i);
    }
}

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
