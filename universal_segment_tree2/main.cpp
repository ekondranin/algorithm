#include <bits/stdc++.h>
//#define endl "\n"
#define FOR(x, y, z) for (int x = (y); x < (z); ++x)
#define FORR(x, y, z) for (int x = (y); x > (z); --x)
#define GET(a, n) for (int __i = 0; __i < (n); ++__i) cin >> a[__i];
#define GETM(a, n, m) for (int __i = 0; __i < (n); ++__i) for (int __j = 0; __j < m; ++__j) cin >> a[__i][__j];
#define PRINTM(a, n, m) for (int __i = 0; __i < (n); ++__i) { for (int __j = 0; __j < m; ++__j) cout << a[__i][__j] << " ";  cout << endl; };
#define PRINT(a, n) for (int __i = 0; __i < (n); ++__i) cout << a[__i] << " ";
#define IT(a) a.begin(), a.end()
#define CASE(a, s) cout << "Case #" << a << ": " << s << endl;
#define DEB(a) cout << #a << " = " << (a) << endl; cout.flush();
#define DEBA(a) for (auto __i: a) cout << __i << " "; cout << endl;
#define IFDEB(b, a) if (b) { cout << #a << " = " << a << endl; cout.flush(); }
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef vector <int> VI;
typedef vector <vector <int>> VVI;
typedef pair <int, int> PII;
const int MOD = 1000000007;
template <class T> typename T::value_type arr_sum(const T& v, int n) { typename T::value_type sum = 0; FOR(i, 0, n) sum += v[i]; return sum; }
struct Sync_stdio { Sync_stdio() { cin.tie(NULL); ios_base::sync_with_stdio(false); } } _sync_stdio;

const int MAXN = 150000;

LL t[4 * MAXN];
LL delta[4 * MAXN];
LL tl[4 * MAXN];
LL tr[4 * MAXN];
int dflag[4 * MAXN];

void push(int v);

void build (vector <LL> &a, int v, int tleft, int tright)
{
    tl[v] = tleft;
    tr[v] = tright;
    if (tleft == tright) {
        if (tleft < a.size()) {
            t[v] = a[tleft];
        }
    } else {
        int tmid = (tleft + tright) / 2;
        build (a, v * 2 + 1, tleft, tmid);
        build (a, v * 2 + 2, tmid + 1, tright);
        t[v] = t[v * 2 + 1] + t[v * 2 + 2];
    }
}

void build(vector <LL> &a)
{
    build(a, 0, 0, MAXN - 1);
}

void change(int v, LL x)
{
    t[v] = (tr[v] - tl[v] + 1) * x;
    delta[v] = x;
    dflag[v] = 1;
}

void update(int v, int l, int r, LL x)
{
    if (l > r) {
        return;
    }
    push(v);
    if (l == tl[v] && tr[v] == r) {
        change(v, x);
    } else {
        int tm = (tl[v] + tr[v]) / 2;
        update (v * 2 + 1, l, min(r, tm), x);
        update (v * 2 + 2, max(l, tm + 1), r, x);
        t[v] = t[2 * v + 1] + t[2 * v + 2];
    }
}

void update(int l, int r, int x)
{
    update(0, l, r, x);
}

void push(int v)
{
    if (dflag[v] != 0) {
        if (v * 2 + 1 < 4 * MAXN - 1) {
            change(2 * v + 1, delta[v]);
        }
        if (v * 2 + 2 < 4 * MAXN - 1) {
            change(2 * v + 2, delta[v]);
        }
        delta[v] = 0;
        dflag[v] = 0;
    }
}

LL sum(int v, int l, int r)
{
    if (l > r) {
        return 0;
    }
    push(v);
    if (l == tl[v] && r == tr[v]) {
        return t[v];
    }
    int tm = (tl[v] + tr[v]) / 2;
    return sum (v * 2 + 1, l, min(r, tm))
+ sum (v * 2 + 2, max(l, tm + 1), r);
}

LL sum(int l, int r)
{
    return sum(0, l, r);
}

int main()
{
    int n, k;
    cin >> n >> k;
    vector <LL> a(n);
    build(a);
    FOR (i, 0, k) {
         string s;
         cin >> s;
         if (s == "A") {
             int l, r, x;
             cin >> l >> r >> x;
             --l, --r;
             update(l, r, x);
         } else {
             int l, r;
             cin >> l >> r;
             --l; --r;
             cout << sum(l, r) << endl;
         }
         /*FOR (i, 0, 16) {
             cout << t[i] << " ";
         }
         cout << endl;
         FOR (i, 0, 16) {
             cout << delta[i] << " ";
         }
         cout << endl;*/
    }
}
