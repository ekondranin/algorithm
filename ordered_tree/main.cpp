#include <bits/stdc++.h>
#ifndef M_PI
#define M_PI 3.14159265359
#endif // M_PI
#define endl "\n"
#define FOR(x, y, z) for (int x = (y); x < (z); ++x)
#define FORR(x, y, z) for (int x = (y); x > (z); --x)
#define GET(a, n) for (int __i = 0; __i < (n); ++__i) cin >> a[__i];
#define GETM(a, n, m) for (int __i = 0; __i < (n); ++__i) for (int __j = 0; __j < m; ++__j) cin >> a[__i][__j];
#define PRINTM(a, n, m) for (int __i = 0; __i < (n); ++__i) { for (int __j = 0; __j < m; ++__j) cout << a[__i][__j] << " ";  cout << endl; };
#define PRINT(a, n) for (int __i = 0; __i < (n); ++__i) cout << a[__i] << " ";
#define IT(a) a.begin(), a.end()
#define CASE(a, s) cout << "Case #" << a << ": " << s << endl;
#define DEB(a) cout << #a << " = " << (a) << endl; cout.flush();
#define DEBA(a) for (auto __i: a) cout << __i << " "; cout << endl;
#define IFDEB(b, a) if (b) { cout << #a << " = " << a << endl; cout.flush(); }
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef vector <int> VI;
typedef vector <vector <int>> VVI;
typedef pair <int, int> PII;
const int MOD = 1000000007;
template <class T> typename T::value_type arr_sum(const T& v, int n) { typename T::value_type sum = 0; FOR(i, 0, n) sum += v[i]; return sum; }
struct Sync_stdio { Sync_stdio() { cin.tie(NULL); ios_base::sync_with_stdio(false); } } _sync_stdio;

#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#include <ext/pb_ds/detail/standard_policies.hpp>
using namespace __gnu_pbds;

template <class T> using ordered_set = tree<
T,
null_type,
less<T>,
rb_tree_tag,
tree_order_statistics_node_update>;

/*

    ordered_set X;
    X.insert(1);
    X.insert(2);
    X.insert(4);
    X.insert(8);
    X.insert(16);

    cout<<*X.find_by_order(1)<<endl; // 2
    cout<<*X.find_by_order(2)<<endl; // 4
    cout<<*X.find_by_order(4)<<endl; // 16
    cout<<(end(X)==X.find_by_order(6))<<endl; // true

    cout<<X.order_of_key(-5)<<endl;  // 0
    cout<<X.order_of_key(1)<<endl;   // 0
    cout<<X.order_of_key(3)<<endl;   // 2
    cout<<X.order_of_key(4)<<endl;   // 2
    cout<<X.order_of_key(400)<<endl; // 5
*/


struct Z
{
    LL l;
    LL r;
    int operator < (const Z &a) const { return r < a.l; }
};

int main()
{
    freopen("river.in", "r", stdin);
    freopen("river.out", "w", stdout);
    int n, p;
    cin >> n >> p;
    ordered_set <Z> s;
    vector <LL> a(n);
    GET(a, n);
    LL sum = 0;
    LL ps = 0;
    FOR (i, 0, a.size()) {
        Z t = {ps, ps + a[i] - 1};
        s.insert(t);
        ps += a[i];
        sum += a[i] * a[i];
    }
    int k;
    cin >> k;
    FOR (i, 0, k) {
        cout << sum << endl;
        int e, v;
        cin >> e >> v;
        --v;
        if (e == 1) {
            auto it = s.find_by_order(v);
            auto it2 = it;
            auto it3 = it;
            sum -= (it->r - it->l + 1) * (it->r - it->l + 1);
            if (it == s.begin()) {
                ++it2;
                sum -= (it2->r - it2->l + 1) * (it2->r - it2->l + 1);
                Z new2 = *it2;
                s.erase(it2);
                new2.l = it->l;
                s.erase(it);
                s.insert(new2);
                sum += (new2.r - new2.l + 1) * (new2.r - new2.l + 1);
                continue;
            }
            ++it2;
            --it3;
            if (it2 == s.end()) {
                sum -= (it3->r - it3->l + 1) * (it3->r - it3->l + 1);
                Z new3 = *it3;
                s.erase(it3);
                new3.r = it->r;
                s.erase(it);
                s.insert(new3);
                sum += (new3.r - new3.l + 1) * (new3.r - new3.l + 1);
                continue;
            }
            int a = (it->r - it->l + 1) / 2;
            int b = (it->r - it->l + 1) - a;
            s.erase(it);
            sum -= (it3->r - it3->l + 1) * (it3->r - it3->l + 1);
            sum -= (it2->r - it2->l + 1) * (it2->r - it2->l + 1);
            Z new2 = *it2;
            s.erase(it2);
            Z new3 = *it3;
            s.erase(it3);
            new2.l -= b;
            new3.r += a;
            s.insert(new2);
            s.insert(new3);
            sum += (new3.r - new3.l + 1) * (new3.r - new3.l + 1);
            sum += (new2.r - new2.l + 1) * (new2.r - new2.l + 1);
        } else {
            auto it = s.find_by_order(v);
            sum -= (it->r - it->l + 1) * (it->r - it->l + 1);
            int a = (it->r - it->l + 1) / 2;
            int b = (it->r - it->l + 1) - a;
            Z new2 = *it;
            Z new3 = *it;
            s.erase(it);
            new2.l += a;
            new3.r -= b;
            s.insert(new2);
            s.insert(new3);
            sum += (new3.r - new3.l + 1) * (new3.r - new3.l + 1);
            sum += (new2.r - new2.l + 1) * (new2.r - new2.l + 1);
        }
    }
    cout << sum << endl;
}
