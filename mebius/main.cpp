#include <iostream>

using namespace std;

int main()
{
    int C = 10000001;
    vector <int> lp(C + 1);
    vector <int> pr;
    FOR (i, 2, C + 1) {
        if (lp[i] == 0) {
            lp[i] = i;
            pr.push_back(i);
        }
        for (int j = 0; j < int(pr.size()) && pr[j] <= lp[i] && i * pr[j] <= C; ++j) {
            lp[i * pr[j]] = pr[j];
        }
    }
    vector <int> meb(C);
    FOR (i, 2, C) {
        if (i / lp[i] == 1) {
            meb[i] = -1;
        } else if (__gcd(i / lp[i], lp[i]) != 1) {
           meb[i] = 0;
        } else {
           meb[i] = meb[i / lp[i]] * meb[lp[i]];
        }
    }
    return 0;
}
