#include <iostream>

using namespace std;

long long
ternary_search(long long mini, long long maxi, long long (*f)(long long t))
{
    while (maxi - mini > 2) {
        int left = ((long long)2 * mini + maxi) / 3;
        int right = ((long long)2 * maxi + mini) / 3;
        if (f(left) < f(right)) {
            maxi = right;
        } else {
            mini = left;
        }
    }
    return min(min(f(mini), f(mini + 1)), f(mini + 2));
}

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
