#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Dint
{
private:
    vector <unsigned> v;
    int sign = 0;
public:
    explicit operator unsigned() { return v[0]; }
    explicit operator int() { return v[0]; }
    Dint(unsigned x) { v.push_back(x); }
    Dint(int x) { v.push_back(abs(x)); sign = x < 0;}
    Dint() { v.push_back(0); }
    Dint operator = (const Dint &a) { v = a.v; sign = a.sign; return *this; }
    int operator == (const Dint &a) const { return (a.v == v) && (a.sign == sign); }
    int operator < (const Dint &a) const
    {
        if (sign == 1 && a.sign == 0) {
            return 1;
        }
        if (sign == 0 && a.sign == 1) {
            return 0;
        }
        if (v.size() < a.v.size()) {
            return 1;
        }
        if (v.size() > a.v.size()) {
            return 0;
        }
        for (int i = v.size() - 1; i >= 0; --i) {
            if ((v[i] < a.v[i] && !sign) || (v[i] > a.v[i] && sign)) {
                return 1;
            }
            if ((v[i] > a.v[i] && !sign) || (v[i] < a.v[i] && sign)) {
                return 0;
            }
        }
        return 0;
    }
    int operator > (const Dint &a) const { return a < *this; }
    int operator <= (const Dint &a) const { return (*this < a) || (*this == a); }
    int operator >= (const Dint &a) const { return (a < *this) || (*this == a); }
    friend Dint abs (const Dint &a)
    {
        Dint c = a;
        c.sign = 0;
        return c;
    }
    friend Dint dplus (const Dint &a, const Dint &b)
    {
        Dint c;
        c.v.resize(0);
        unsigned t = 0;
        for (int i = 0; i < max(int(a.v.size()), int(b.v.size())); ++i) {
            unsigned x = i < int(a.v.size()) ? a.v[i]: 0;
            unsigned y = i < int(b.v.size()) ? b.v[i]: 0;
            unsigned long long t2 = (unsigned long long)x + y + t;
            c.v.push_back(t2 % 0x100000000);
            t = t2 / 0x100000000;
        }
        if (t != 0)
            c.v.push_back(t);
        if (c.v.size() == 0)
            c.v.push_back(0);
        return c;
    }
    friend Dint dminus (const Dint &a, const Dint &b)
    {
        Dint c;
        c.v.resize(0);
        int t = 0;
        for (int i = 0; i < int(a.v.size()); ++i) {
            unsigned x = a.v[i];
            unsigned y = i < int(b.v.size()) ? b.v[i]: 0;
            long long t2 = (long long)x - y - t;
            if (t2 < 0) {
                t2 += 0x100000000;
                t = 1;
            } else {
                t = 0;
            }
            c.v.push_back(t2);
        }
        int s = c.v.size();
        while (c.v[s - 1] == 0) {
            --s;
        }
        c.v.resize(s);
        if (c.v.size() == 0) {
            c.v.push_back(0);
        }
        return c;
    }
    friend Dint dmulshort (const Dint &a, unsigned b)
    {
        Dint c;
        c.v.resize(0);
        unsigned t = 0;
        for (int i = 0; i < int(a.v.size()); ++i) {
            unsigned x = i < int(a.v.size()) ? a.v[i]: 0;
            unsigned long long t2 = (unsigned long long)x * b + t;
            c.v.push_back(t2 % 0x100000000);
            t = t2 / 0x100000000;
        }
        if (t != 0)
            c.v.push_back(t);
        if (c.v.size() == 0)
            c.v.push_back(0);
        return c;
    }
    friend Dint ddivshort (const Dint &a, unsigned b)
    {
        Dint c;
        c.v.resize(a.v.size());
        unsigned t = 0;
        for (int i = a.v.size() - 1; i >= 0; --i) {
            unsigned long long x = a.v[i];
            unsigned t2 = (x + ((unsigned long long)t << 32)) / b;
            t = (x + ((unsigned long long)t << 32)) % b;
            c.v[i] = t2;
        }
        if (c.v[c.v.size() - 1] == 0) {
            c.v.resize(c.v.size() - 1);
        }
        if (c.v.size() == 0) {
            c.v.push_back(0);
        }
        return c;
    }
    friend unsigned dmodshort (const Dint &a, unsigned b)
    {
        unsigned t = 0;
        for (int i = a.v.size() - 1; i >= 0; --i) {
            unsigned long long x = a.v[i];
            t = (x + ((unsigned long long)t << 32)) % b;
        }
        return t;
    }
    Dint operator - () const { Dint c = *this; c.sign ^= 1; return c; }
    Dint operator + (const Dint &a) const
    {
        if (*this >= 0 && a >= 0) {
            return dplus(*this, a);
        }
        if (*this < 0 && a < 0) {
            return -dplus(-*this, -a);
        }
        if (*this >= 0 && a < 0) {
            if (*this >= -a)
                return dminus(*this, -a);
            return -dminus(a, -*this);
        }
        if (a >= -*this)
            return dminus(a, -*this);
        return -dminus(-*this, a);
    }
    Dint operator - (const Dint &a) const {
        if (*this >= 0 && a >= 0) {
            if (*this >= a)
                return dminus(*this, a);
            return -dminus(a, *this);
        }
        if (*this < 0 && a < 0) {
            if (-*this >= -a)
                return -dminus(-*this, -a);
            return dminus(-a, -*this);
        }
        if (*this >= 0 && a < 0) {
            return dplus(*this, -a);
        }
        return -dplus(-*this, a);
    }
    Dint operator * (int a) const {
        if ((a >= 0) ^ (*this < 0)) {
            return dmulshort(*this, a);
        }
        return -dmulshort(abs(*this), abs(a));
    }
    Dint operator * (unsigned a) const {
        if (a >= 0) {
            return dmulshort(*this, a);
        }
        return -dmulshort(*this, -a);
    }
    Dint operator / (int a) const {
        if ((a >= 0) ^ (*this < 0)) {
            return ddivshort(*this, a);
        }
        return -ddivshort(abs(*this), abs(a));
    }
    Dint operator / (unsigned a) const {
        if (a >= 0) {
            return ddivshort(*this, a);
        }
        return -ddivshort(*this, -a);
    }
    int operator % (int a) const {
        if ((a >= 0) ^ (*this < 0)) {
            return dmodshort(*this, a);
        }
        return -dmodshort(abs(*this), abs(a));
    }
    unsigned operator % (unsigned a) const {
        if (a >= 0) {
            return dmodshort(*this, a);
        }
        return -dmodshort(*this, -a);
    }
    Dint operator += (const Dint &a) {
        return *this = *this + a;
    }
    Dint operator -= (const Dint &a) {
        return *this = *this - a;
    }
    Dint operator *= (int a) {
        return *this = *this * a;
    }
    Dint operator *= (unsigned a) {
        return *this = *this * a;
    }
    Dint operator /= (int a) {
        return *this = *this / a;
    }
    Dint operator /= (unsigned a) {
        return *this = *this / a;
    }
    friend istream & operator >> (istream &is, Dint &a)
    {
        string s;
        int ss = 0;
        is >> s;
        if (*s.begin() == '-') {
            ss = 1;
            s.erase(s.begin());
        }
        a = 0;
        for (int i = 0; i < int(s.size()); ++i) {
            a *= 10;
            a += s[i] - '0';
        }
        if (ss)
            a.sign = 1;
        return is;
    }
    friend ostream & operator << (ostream &os, const Dint &a)
    {
        Dint c = a;
        string d;
        int s = c.sign;
        c = abs(c);
        if (c == 0) {
            cout << "0";
            return os;
        }
        while (c > 0) {
            string t = to_string(c % 1000000000);
            while (t.size() != 9) {
                t = '0' + t;
            }
            d = t + d;
            c /= 1000000000;
        }
        while (*d.begin() == '0') {
            d.erase(d.begin());
        }
        if (d.size() == 0) {
            d = "0";
        }
        if (s == 1)
            cout << "-";
        cout << d;
        return os;
    }
};

int main()
{
    Dint a, b;
    cin >> a;
    cout << a - b;
    return 0;
}
