#include <iostream>

using namespace std;

int gcd_ext(int a, int b, int &x, int &y) {
    if (a == 0) {
        x = 0; y = 1;
        return b;
    }
    int x1, y1;
    int d = gcd_ext(b % a, a, x1, y1);
    x = y1 - (b / a) * x1;
    y = x1;
    return d;
}

LL inv(LL t, LL mod)
{
    if (t == 0) {
        return 0;
    }
    int x;
    int y;
    gcd_ext(t, mod, x, y);
    return (x % mod + mod) % mod;
}

vector <LL> fact(1000002);
vector <LL> inv_fact(1000002);

int comb(int n, int k)
{
    if (n < k) {
        return 0;
    }
    LL res = fact[n];
    res *= inv_fact[k];
    res %= MOD;
    res *= inv_fact[n - k];
    res %= MOD;
    return res;
}

void init_comb()
{
    fact[0] = 1;
    inv_fact[0] = 1;
    FOR (i, 1, fact.size()) {
        fact[i] = fact[i - 1] * i;
        fact[i] %= MOD;
        inv_fact[i] = inv(fact[i], MOD);
    }
}

int main()
{
    init_comb();
    cout << "Hello world!" << endl;
    return 0;
}
