#include <iostream>
#include <vector>

using namespace std;

long long bsearch_answer(long long l, long long r, int (*f)(long long t))
{
    while (l < r) {
        long long t = (l + r) / 2;
        int p = f(t);
        if (!p) {
            l = t;
            break;
        } else if (p > 0) {
            r = t;
        } else {
            l = t + 1;
        }
    }
    return l;
}

enum {GREATER = -1, LOWER = 1};

vector <int> v;
int k;

int f(long long t)
{
    int res = 1;
    int a = v[0];
    for (int i = 1; i < v.size(); ++i) {
        if (v[i] - a >= t) {
            ++res;
            a = v[i];
        }
    }
    if (res < k)
        return 1;
    return -1;
}

int main()
{
    int n;
    cin >> n >> k;
    for (int i = 0; i < n; ++i) {
        int tmp;
        cin >> tmp;
        v.push_back(tmp);
    }
    cout << bsearch_answer(1, 1000000000, f) - 1;
    return 0;
}
