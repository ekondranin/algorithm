#include <bits/stdc++.h>
#define endl "\n"
#define FOR(x, y, z) for (int x = (y); x < (z); ++x)
#define FORR(x, y, z) for (int x = (y); x > (z); --x)
#define GET(a, n) for (int i = 0; i < (n); ++i) cin >> a[i];
#define GETM(a, n, m) for (int i = 0; i < (n); ++i) for (int j = 0; j < m; ++j) cin >> a[i][j];
#define PRINTM(a, n, m) for (int i = 0; i < (n); ++i) { for (int j = 0; j < m; ++j) cout << a[i][j] << " ";  cout << endl; };
#define PRINT(a, n) for (int i = 0; i < (n); ++i) cout << a[i] << " ";
#define IT(a) a.begin(), a.end()
#define DEB(a) cout << #a << " = " << a << endl; cout.flush();
#define DEBA(a) for (auto i: a) cout << i << " "; cout << endl;
#define IFDEB(b, a) if (b) { cout << #a << " = " << a << endl; cout.flush(); }
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef vector <int> VI;
typedef vector <vector <int>> VVI;
typedef pair <int, int> PII;
const int MOD = 1000000007;
template <class T> typename T::value_type arr_sum(const T& v, int n) { typename T::value_type sum = 0; FOR(i, 0, n) sum += v[i]; return sum; }
struct Sync_stdio { Sync_stdio() { cin.tie(NULL); ios_base::sync_with_stdio(false); } } _sync_stdio;

template <class T> class MinSegmentSafe
{
    stack <pair <T, T>> s1, s2;
public:
    T top() {
        T current_minimum;
        if (s1.empty() && s2.empty()) {
            return 0;
        }
        if (s1.empty() || s2.empty()) {
        	current_minimum = s1.empty() ? s2.top().second : s1.top().second;
        } else {
	        current_minimum = min(s1.top().second, s2.top().second);
        }
        return current_minimum;
    }
    void push(T new_element) {
        T minima = s1.empty() ? new_element : min(new_element, s1.top().second);
        s1.push(make_pair(new_element, minima));
    }
    void pop() {
        if (s2.empty() && s1.empty()) {
            return;
        }
        if (s2.empty()) {
        	while (!s1.empty()) {
		        T element = s1.top().first;
        		s1.pop();
        		T minima = s2.empty() ? element : min (element, s2.top().second);
		        s2.push (make_pair (element, minima));
	        }
        }
        s2.pop();
    }
};

template <class T> class MinSegment
{
    stack <pair <T, T>> s1, s2;
public:
    T top() {
        T current_minimum;
        if (s1.empty() || s2.empty()) {
        	current_minimum = s1.empty() ? s2.top().second : s1.top().second;
        } else {
	        current_minimum = min(s1.top().second, s2.top().second);
        }
        return current_minimum;
    }
    void push(T new_element) {
        T minima = s1.empty() ? new_element : min(new_element, s1.top().second);
        s1.push(make_pair(new_element, minima));
    }
    void pop() {
        if (s2.empty()) {
        	while (!s1.empty()) {
		        T element = s1.top().first;
        		s1.pop();
        		T minima = s2.empty() ? element : min (element, s2.top().second);
		        s2.push (make_pair (element, minima));
	        }
        }
        s2.pop();
    }
};

int main()
{
    MinSegment <LL> x;
    string s;
    int y;
    while (cin >> s) {
        if (s == "POP") {
            x.pop();
            continue;
        }
        if (s == "TOP") {
            cout << x.top() << endl;
            cout.flush();
            continue;
        }
        cin >> y;
        x.push(y);
    }
}
