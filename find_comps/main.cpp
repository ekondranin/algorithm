#include <iostream>
#include <vector>

using namespace std;

namespace Comps_dfs
{
    void
    dfs(const std::vector <std::vector <int>> &g, std::vector <int> &busy, int t, std::vector <int> &res)
    {
        if (busy[t])
            return;
        busy[t] = 1;
        for(int i = 0; i < int(g[t].size()); ++i) {
            dfs(g, busy, g[t][i], res);
        }
        res.push_back(t);
    }
};

void
find_comps(const std::vector <std::vector <int>> &g, std::vector <std::vector <int>> &comps)
{
    using namespace Comps_dfs;
    vector <int> busy(g.size(), 0);
    for (int i = 0; i < int(g.size()); ++i) {
        vector <int> res;
        dfs(g, busy, i, res);
        if (res.size()) {
            comps.push_back(res);
        }
    }
}

void
get_graph(vector <vector <int>> &g)
{
    int n, m;
    cin >> n >> m;
    g.resize(n);
    for (int i = 0; i < m; ++i) {
        int t1, t2;
        cin >> t1 >> t2;
        --t1;
        --t2;
        g[t1].push_back(t2);
        g[t2].push_back(t1);
    }
}

int main()
{
    vector <vector <int>> g;
    get_graph(g);
    vector <vector <int>> comps;
    find_comps(g, comps);
    cout << comps.size() << endl;
    for (int i = 0; i < int(comps.size()); ++i) {
        cout << comps[i].size() << endl;
        for (auto j: comps[i]) {
            cout << j + 1 << " ";
        }
        cout << endl;
    }
    return 0;
}
