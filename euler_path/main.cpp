#include <iostream>

using namespace std;

vector <multiset <int>> g;

void
euler_path(int t)
{
    stack <int> st;
    st.push(t);
    while (!st.empty()) {
        int p = st.top();
        if (g[p].empty()) {
            res.push_back(p);
            st.pop();
        } else {
            int s = *g[p].begin();
            auto it = g[p].find(s);
            if (it != g[p].end()) {
                g[p].erase(it);
            }
            it = g[s].find(p);
            if (it != g[s].end()) {
                g[s].erase(it);
            }
            st.push(s);
        }
    }
}

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
