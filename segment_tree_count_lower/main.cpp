#include <iostream>

using namespace std;

#include <bits/stdc++.h>
#ifndef M_PI
#define M_PI 3.14159265359
#endif // M_PI
#define endl "\n"
#define FOR(x, y, z) for (int x = (y); x < (z); ++x)
#define FORR(x, y, z) for (int x = (y); x > (z); --x)
#define GET(a, n) for (int __i = 0; __i < (n); ++__i) cin >> a[__i];
#define GETM(a, n, m) for (int __i = 0; __i < (n); ++__i) for (int __j = 0; __j < m; ++__j) cin >> a[__i][__j];
#define PRINTM(a, n, m) for (int __i = 0; __i < (n); ++__i) { for (int __j = 0; __j < m; ++__j) cout << a[__i][__j] << " ";  cout << endl; };
#define PRINT(a, n) for (int __i = 0; __i < (n); ++__i) cout << a[__i] << " ";
#define IT(a) a.begin(), a.end()
#define CASE(a, s) cout << "Case #" << a << ": " << s << endl;
#define DEB(a) cout << #a << " = " << (a) << endl; cout.flush();
#define DEBA(a) for (auto __i: a) cout << __i << " "; cout << endl;
#define IFDEB(b, a) if (b) { cout << #a << " = " << a << endl; cout.flush(); }
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef vector <int> VI;
typedef vector <vector <int>> VVI;
typedef pair <int, int> PII;
const int MOD = 1000000007;
template <class T> typename T::value_type arr_sum(const T& v, int n) { typename T::value_type sum = 0; FOR(i, 0, n) sum += v[i]; return sum; }
struct Sync_stdio { Sync_stdio() { cin.tie(NULL); ios_base::sync_with_stdio(false); } } _sync_stdio;

const int MAXN = 2000000;

vector <int> t[4 * MAXN];

void build (const vector <int> &a, int v, int tl, int tr) {
	if (tl == tr)
		t[v] = vector <int> (1, a[tl]);
	else {
		int tm = (tl + tr) / 2;
		build (a, v * 2 + 1, tl, tm);
		build (a, v * 2 + 2, tm + 1, tr);
		merge (t[v * 2 + 1].begin(), t[v * 2 + 1].end(), t[v * 2 + 2].begin(), t[v * 2 + 2].end(),
			back_inserter (t[v]));
	}
}

int query (int v, int tl, int tr, int l, int r, int x) { //<
	if (l > r)
		return 0;
	if (l == tl && tr == r) {
		auto pos = lower_bound(t[v].begin(), t[v].end(), x);
        return pos - t[v].begin();
	}
	int tm = (tl + tr) / 2;
	return query (v * 2 + 1, tl, tm, l, min(r, tm), x) +
		query (v * 2 + 2, tm + 1, tr, max(l, tm + 1), r, x);
}

template <class T>
class SegmentTreeCount
{
    vector <vector <T>> t;
    int maxn = 1000000;
    void build (const vector <T> &a, int v, int tl, int tr)
    {
        if (tl == tr) {
            t[v] = vector <T> (1, a[tl]);
        } else {
            int tm = (tl + tr) / 2;
            build (a, v * 2 + 1, tl, tm);
            build (a, v * 2 + 2, tm + 1, tr);
            merge (t[v * 2 + 1].begin(), t[v * 2 + 1].end(), t[v * 2 + 2].begin(), t[v * 2 + 2].end(),
back_inserter (t[v]));
        }
    }
    int query (int v, int tl, int tr, int l, int r, int x) const
    { //<
        if (l > r)
            return 0;
        if (l == tl && tr == r) {
            auto pos = lower_bound(t[v].begin(), t[v].end(), x);
            return pos - t[v].begin();
        }
        int tm = (tl + tr) / 2;
        return query (v * 2 + 1, tl, tm, l, min(r, tm), x) +
query (v * 2 + 2, tm + 1, tr, max(l, tm + 1), r, x);
    }
    inline int query(int l, int r, int x) const
    {
        return query(0, 0, maxn - 1, l, r, x);
    }
public:
    SegmentTreeCount (const vector <T> &v)
    {
        maxn = v.size();
        t.resize(4 * maxn);
        build(v, 0, 0, maxn - 1);
    }
    inline int lower(int l, int r, int x) const
    {
        return query(l, r, x);
    }
    inline int great_eq(int l, int r, int x) const
    {
        return r - l + 1 - query(l, r, x);
    }
    inline int lower_eq(int l, int r, int x) const
    {
        return query(l, r, x + 1);
    }
    inline int greater(int l, int r, int x) const
    {
        return r - l + 1 - query(l, r, x + 1);
    }
    inline int equal(int l, int r, int x) const
    {
        return lower_eq(l, r, x) - lower(l, r, x);
    }
};

int main()
{
    vector <int> v = {2, 3, 1, 3, 3, 1, 0, 100, 1, 3};
    SegmentTreeCount <int> t(v);
    cout << t.lower(1, 4, 3) << endl;
    cout << t.lower(1, 4, 2) << endl;
    cout << t.lower(1, 4, 4) << endl;
    cout << t.lower(1, 4, 1) << endl;
    cout << t.lower(1, 4, 0) << endl;
    cout << t.greater(1, 4, 3) << endl;
    cout << t.great_eq(1, 4, 3) << endl;
    cout << t.lower_eq(1, 4, 3) << endl;
    cout << t.equal(1, 4, 3) << endl;
    return 0;
}
