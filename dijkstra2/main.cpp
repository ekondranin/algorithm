#include <iostream>
#include <queue>

using namespace std;

vector <long long> d;

long long
dijkstra(vector <vector <pair<long long, long long>>> &g, int a, int b = -1)
{
    struct Cmp
    {
        int operator() (pair<long long, long long> x, pair<long long, long long> y) { return x.second > y.second; }
    };
    priority_queue <pair<long long, long long>, vector <pair<long long, long long>>, Cmp> pq;
    d.resize(g.size());
    for(int i = 0; i < g.size(); ++i) {
        d[i] = -1;
    }
    pq.push(make_pair(a, 0));
    d[a] = 0;
    while (!pq.empty()) {
        pair<long long, long long> t = pq.top();
        pq.pop();
        if (t.second > d[t.first])
            continue;
        if (t.first == b)
            break;
        for (int i = 0; i < g[t.first].size(); ++i) {
            if (d[g[t.first][i].first] == -1 || d[t.first] + g[t.first][i].second < d[g[t.first][i].first]) {
                d[g[t.first][i].first] = d[t.first] + g[t.first][i].second;
                pq.push(make_pair(g[t.first][i].first, d[g[t.first][i].first]));
            }
        }
    }
    if (b == -1)
        return 0;
    return d[b];
}

int main()
{
    int num;
    cin >> num;
    for (int i = 0; i < num; ++i) {
        int n, m;
        cin >> n >> m;
        vector <vector <pair <long long, long long>>> g(n);
        for (int j = 0; j < m; ++j) {
            int a, b, c;
            cin >> a >> b >> c;
            g[a].push_back(make_pair(b, c));
            g[b].push_back(make_pair(a, c));
        }
        /*vector <long long> d;*/
        int start;
        cin >> start;
        dijkstra(g/*, d*/, start);
        for (auto i: d) {
            if (i == -1)
                cout << 2009000999 << " ";
            else
                cout << i << " ";
        }
        cout << endl;
    }
    return 0;
}
