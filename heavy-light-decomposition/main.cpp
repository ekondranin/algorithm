#include <bits/stdc++.h>
#ifndef M_PI
#define M_PI 3.14159265359
#endif // M_PI
#define endl "\n"
#define FOR(x, y, z) for (int x = (y); x < (z); ++x)
#define FORR(x, y, z) for (int x = (y); x > (z); --x)
#define GET(a, n) for (int __i = 0; __i < (n); ++__i) cin >> a[__i];
#define GETM(a, n, m) for (int __i = 0; __i < (n); ++__i) for (int __j = 0; __j < m; ++__j) cin >> a[__i][__j];
#define PRINTM(a, n, m) for (int __i = 0; __i < (n); ++__i) { for (int __j = 0; __j < m; ++__j) cout << a[__i][__j] << " ";  cout << endl; };
#define PRINT(a, n) for (int __i = 0; __i < (n); ++__i) cout << a[__i] << " ";
#define IT(a) a.begin(), a.end()
#define SQR(x) (x) * (x)
#define CASE(a, s) cout << "Case #" << a << ": " << s << endl;
#define DEB(a) cout << #a << " = " << (a) << endl; cout.flush();
#define DEBA(a) for (auto __i: a) cout << __i << " "; cout << endl; cout.flush();
#define IFDEB(b, a) if (b) { cout << #a << " = " << (a) << endl; cout.flush(); }
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef vector <int> VI;
typedef vector <vector <int>> VVI;
typedef pair <int, int> PII;
const int MOD = 1000000007;
template <class T> typename T::value_type arr_sum(const T& v, int n) { typename T::value_type sum = 0; FOR(i, 0, n) sum += v[i]; return sum; }
struct Sync_stdio { Sync_stdio() { cin.tie(NULL); ios_base::sync_with_stdio(false); } } _sync_stdio;

template <class T> class SegmentTree
{
    int maxn = 1000000;
    vector <T> t;
    void build(const vector <T> &a, int v, int tl, int tr) {
        if (tl == tr) {
            t[v] = a[tl];
        } else {
            int tm = (tl + tr) / 2;
            build (a, v * 2 + 1, tl, tm);
            build (a, v * 2 + 2, tm + 1, tr);
            t[v] = max(t[v * 2 + 1], t[v * 2 + 2]);
        }
    }
    T sum(int v, int tl, int tr, int l, int r) {
        if (l > r) {
            return INT_MIN;
        }
        if (l == tl && r == tr) {
            return t[v];
        }
        int tm = (tl + tr) / 2;
        return max(sum(v * 2 + 1, tl, tm, l, min(r, tm)),
sum(v * 2 + 2, tm + 1, tr, max(l, tm + 1), r));
    }
    void update(int v, int tl, int tr, int pos, T new_val) {
        if (tl == tr) {
            t[v] += new_val;
        } else {
            int tm = (tl + tr) / 2;
            if (pos <= tm) {
                update (v * 2 + 1, tl, tm, pos, new_val);
            } else {
                update (v * 2 + 2, tm + 1, tr, pos, new_val);
            }
            t[v] = max(t[v * 2 + 1], t[v * 2 + 2]);
        }
    }
public:
    SegmentTree (int n)
    {
        maxn = n;
        t.resize(4 * maxn);
    }
    int size() { return maxn; }
    SegmentTree () {}
    SegmentTree (const vector <T> &a) {
        maxn = a.size();
        t.resize(4 * maxn);
        build(a, 0, 0, maxn - 1);
    }
    void update(int pos, T add)
    {
        update(0, 0, maxn - 1, pos, add);
    }
    T sum(int l, int r)
    {
        return sum(0, 0, maxn - 1, l, r);
    }
};


class HeavyLightDecomp
{
    vector <SegmentTree <int>> tr;
    vector <int> next;
    vector <vector <int>> up;
    int timer = 0;
    int tr_size = 0;
    vector <int> used;
    vector <int> tin;
    vector <int> tout;
    vector <int> par;
    vector <int> s;
    vector <PII> pos;
    const vector <vector <int>> &g;
    void dfs(int t, int p = 0)
    {
        s[t] = 1;
        used[t] = 1;
        par[t] = p;
        tin[t] = ++timer;
        up[t][0] = p;
        FOR (i, 1, int(up[t].size())) {
            up[t][i] = up[up[t][i - 1]][i - 1];
        }
        for (auto i: g[t]) {
            if (used[i]) {
                continue;
            }
            dfs(i, t);
            s[t] += s[i];
        }
        tout[t] = ++timer;
    }
    int upper(int a, int b)
    {
        return tout[a] >= tout[b] && tin[a] <= tin[b];
    }
    int lca(int a, int b)
    {
        if (upper(a, b)) {
            return a;
        }
        if (upper(b, a)) {
            return b;
        }
        FORR (i, up[a].size() - 1, -1) {
            if (!upper(up[a][i], b)) {
                a = up[a][i];
            }
        }
        return up[a][0];
    }
    inline int light(int i, int j)
    {
        return 2 * s[j] < s[i];
    }
    inline int heavy(int i, int j)
    {
        return 2 * s[j] >= s[i];
    }
    void go_up(int x, int p)
    {
        //cout << "UP " << tr_size - 1 << ": " << x << " " << p << endl;
        pos[x] = {tr_size - 1, p};
        int flag = 0;
        for (auto i: g[x]) {
            if (upper(x, i)) {
                continue;
            }
            if (heavy(i, x)) {
                flag = 1;
                go_up(i, p + 1);
            } else {
                next[tr_size - 1] = i;
            }
        }
        if (!flag) {
            tr[tr_size - 1] = SegmentTree <int> (p + 1);
        }
    }
    void init()
    {
        timer = 0;
        pos.resize(g.size());
        par.resize(g.size());
        used.assign(g.size(), 0);
        tin.resize(g.size());
        tout.resize(g.size());
        up.resize(g.size());
        s.resize(g.size());
        int l = 1;
        while ((1 << l) <= int(g.size())) {
            ++l;
        }
        for (auto &i: up) {
           i.resize(l + 1);
        }
        dfs(0);
        FOR (i, 0, int(g.size())) {
            int flag = 1;
            for (auto j: g[i]) {
                if (upper(j, i)) {
                    continue;
                }
                if (heavy(i, j)) {
                    flag = 0;
                    break;
                }
            }
            if (!flag) {
                continue;
            }
            ++tr_size;
            tr.resize(tr_size);
            next.resize(tr_size);
            next[tr_size - 1] = -1;
            go_up(i, 0);
        }
    }
public:
    HeavyLightDecomp(const vector <vector <int>> &tree): g(tree)
    {
        init();
    }
    void update(int x, int val)
    {
        tr[pos[x].first].update(pos[x].second, val);
    }
    int sum(int x, int y)
    {
        int p = lca(x, y);
        int ans = INT_MIN;
        if (pos[x].first == pos[p].first) {
            ans = max(ans, tr[pos[x].first].sum(pos[x].second, pos[p].second));
        } else {
            int t = x;
            while (pos[t].first != pos[p].first) {
                ans = max(ans, tr[pos[t].first].sum(pos[t].second, tr[pos[t].first].size() - 1));
                t = next[pos[t].first];
            }
            ans = max(ans, tr[pos[t].first].sum(pos[t].second, pos[p].second));
        }
        if (pos[y].first == pos[p].first) {
            ans = max(ans, tr[pos[y].first].sum(pos[y].second, pos[p].second - 1));
        } else {
            int t = y;
            while (pos[t].first != pos[p].first) {
                ans = max(ans, tr[pos[t].first].sum(pos[t].second, tr[pos[t].first].size() - 1));
                t = next[pos[t].first];
            }
            ans = max(ans, tr[pos[t].first].sum(pos[t].second, pos[p].second - 1));
        }
        return ans;
    }
};

int main()
{
    int n;
    cin >> n;
    vector <vector <int>> g(n);
    FOR (i, 0, n - 1) {
        int a, b;
        cin >> a >> b;
        --a, --b;
        g[a].push_back(b);
        g[b].push_back(a);
    }
    HeavyLightDecomp h(g);
    int m;
    cin >> m;
    FOR (i, 0, m) {
        string s;
        cin >> s;
        if (s == "I") {
            int v, add;
            cin >> v >> add;
            --v;
            h.update(v, add);
        } else {
            int x, y;
            cin >> x >> y;
            --x, --y;
            cout << h.sum(x, y) << endl;
        }
    }
}
