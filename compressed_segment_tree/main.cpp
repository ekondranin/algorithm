#include <bits/stdc++.h>
#ifndef M_PI
#define M_PI 3.14159265359
#endif // M_PI
#define endl "\n"
#define FOR(x, y, z) for (int x = (y); x < (z); ++x)
#define FORR(x, y, z) for (int x = (y); x > (z); --x)
#define GET(a, n) for (int __i = 0; __i < (n); ++__i) cin >> a[__i];
#define GETM(a, n, m) for (int __i = 0; __i < (n); ++__i) for (int __j = 0; __j < m; ++__j) cin >> a[__i][__j];
#define PRINTM(a, n, m) for (int __i = 0; __i < (n); ++__i) { for (int __j = 0; __j < m; ++__j) cout << a[__i][__j] << " ";  cout << endl; };
#define PRINT(a, n) for (int __i = 0; __i < (n); ++__i) cout << a[__i] << " ";
#define IT(a) a.begin(), a.end()
#define SQR(x) (x) * (x)
#define CASE(a, s) cout << "Case #" << a << ": " << s << endl;
#define DEB(a) cout << #a << " = " << (a) << endl; cout.flush();
#define DEBA(a) for (auto __i: a) cout << __i << " "; cout << endl; cout.flush();
#define IFDEB(b, a) if (b) { cout << #a << " = " << (a) << endl; cout.flush(); }
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef vector <int> VI;
typedef vector <vector <int>> VVI;
typedef pair <int, int> PII;
const int MOD = 1000000007;
template <class T> typename T::value_type arr_sum(const T& v, int n) { typename T::value_type sum = 0; FOR(i, 0, n) sum += v[i]; return sum; }
struct Sync_stdio { Sync_stdio() { cin.tie(NULL); ios_base::sync_with_stdio(false); } } _sync_stdio;

template <class T> class SegmentTree
{
    struct Node
    {
        int l;
        int r;
        int lson;
        int rson;
        T val;
    };
    int maxn = 1000000;
    vector <Node> t;
    T sum(int v, int tl, int tr, int l, int r) {
        if (l > r) {
            return 0;
        }
        if (l == tl && r == tr) {
            return t[v].val;
        }
        int tm = (tl + tr) / 2;
        T s = 0;
        if (t[v].lson != -1) {
            s += sum(t[v].lson, tl, tm, l, min(r, tm));
        }
        if (t[v].rson != -1) {
            s += sum(t[v].rson, tm + 1, tr, max(l, tm + 1), r);
        }
        return s;
    }
    void update(int v, int tl, int tr, int pos, T new_val) {
        if (tl == tr) {
            t[v].val += new_val;
        } else {
            int tm = (tl + tr) / 2;
            if (pos <= tm) {
                if (t[v].lson == -1) {
                    t.push_back({tl, tm, -1, -1, 0});
                    t[v].lson = t.size() - 1;
                }
                update(t[v].lson, tl, tm, pos, new_val);
            } else {
                if (t[v].rson == -1) {
                    t.push_back({tm + 1, tr, -1, -1, 0});
                    t[v].rson = t.size() - 1;
                }
                update(t[v].rson, tm + 1, tr, pos, new_val);
            }
            t[v].val = 0;
            if (t[v].lson != -1) {
                t[v].val += t[t[v].lson].val;
            }
            if (t[v].rson != -1) {
                t[v].val += t[t[v].rson].val;
            }
        }
    }
public:
    SegmentTree () {}
    SegmentTree (int n)
    {
        maxn = n;
        t.push_back({0, maxn - 1, -1, -1, 0});
    }
    void update(int pos, T add)
    {
        update(0, 0, maxn - 1, pos, add);
    }
    T sum(int l, int r)
    {
        return sum(0, 0, maxn - 1, l, r);
    }
};

int main()
{
    SegmentTree <LL> t(1000);
    while (1) {
        int x;
        cin >> x;
        if (x == 1) {
            int pos, val;
            cin >> pos >> val;
            t.update(pos, val);
        } else {
            int l, r;
            cin >> l >> r;
            cout << t.sum(l, r) << endl;
            cout.flush();
        }
    }
}
