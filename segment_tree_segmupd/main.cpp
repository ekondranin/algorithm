#include <iostream>

using namespace std;

const int MAXN = 1000000;

int t[4*MAXN];

void build (int a[], int v, int tl, int tr) {
    if (tl == tr)
        t[v] = a[tl];
    else {
        int tm = (tl + tr) / 2;
        build (a, v*2 + 1, tl, tm);
        build (a, v*2 + 2, tm+1, tr);
    }
}

void update (int v, int tl, int tr, int l, int r, int add) {
	if (l > r)
		return;
	if (l == tl && tr == r)
		t[v] += add;
	else {
		int tm = (tl + tr) / 2;
		update (v*2 + 1, tl, tm, l, min(r,tm), add);
		update (v*2 + 2, tm+1, tr, max(l,tm+1), r, add);
	}
}

int get (int v, int tl, int tr, int pos) {
	if (tl == tr)
		return t[v];
	int tm = (tl + tr) / 2;
	if (pos <= tm)
		return t[v] + get (v*2 + 1, tl, tm, pos);
	else
		return t[v] + get (v*2 + 2, tm+1, tr, pos);
}

template <class T> class SegmentTree//?
{
    int maxn = 1000000;
    vector <T> t;
    void build (vector <T> a, int v, int tl, int tr) {
        if (tl == tr) {
            t[v] = a[tl];
        } else {
            int tm = (tl + tr) / 2;
            build (a, v * 2 + 1, tl, tm);
            build (a, v * 2 + 2, tm + 1, tr);
        }
    }
    void update (int v, int tl, int tr, int l, int r, T add) {
	    if (l > r) {
		    return;
	    }
        if (l == tl && tr == r) {
		    t[v] += add;
     	} else {
		    int tm = (tl + tr) / 2;
            update (v * 2 + 1, tl, tm, l, min(r, tm), add);
            update (v * 2 + 2, tm + 1, tr, max(l, tm + 1), r, add);
     	}
    }
    T get (int v, int tl, int tr, int pos) {
	    if (tl == tr) {
            return t[v];
	    }
        int tm = (tl + tr) / 2;
	    if (pos <= tm) {
            return t[v] + get (v * 2 + 1, tl, tm, pos);
	    } else {
            return t[v] + get (v * 2 + 2, tm + 1, tr, pos);
	    }
    }
public:
    T get(int pos) {
        return get(0, 0, maxn - 1, pos);
    }
    void update(int l, int r, T add) {
        update(0, 0, maxn - 1, l, r, add);
    }
    SegmentTree(int n) {
        maxn = n;
        t.resize(4 * maxn);
    }
    SegmentTree(const vector <T> &v) {
        maxn = v.size();
        t.resize(4 * maxn);
        build(v, 0, 0, maxn - 1);
    }
};

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
