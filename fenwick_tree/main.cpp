#include <iostream>

using namespace std;

template <class T> class FenwickTree
{
    int maxn = 1000000;
    vector <T> t;
    T sum(int r)
    {
        int result = 0;
        for (; r >= 0; r = (r & (r + 1)) - 1) {
            result += t[r];
        }
        return result;
    }
    void init(const vector <T> &a)
    {
        init((int) a.size());
        for (unsigned i = 0; i < a.size(); ++i) {
           inc (i, a[i]);
        }
    }
    void init(int n)
    {
        maxn = n;
        t.assign(n, 0);
    }
public:
    FenwickTree (int n)
    {
        maxn = n;
        t.resize(maxn);
    }
    T sum(int l, int r)
    {
        return sum(r) - sum(l - 1);
    }
    void update(int i, int delta)
    {
        for (; i < maxn; i = (i | (i + 1))) {
            t[i] += delta;
        }
    }
};

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
