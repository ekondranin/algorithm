#include <bits/stdc++.h>
//#define endl "\n"
#define FOR(x, y, z) for (int x = (y); x < (z); ++x)
#define FORR(x, y, z) for (int x = (y); x > (z); --x)
#define GET(a, n) for (int __i = 0; __i < (n); ++__i) cin >> a[__i];
#define GETM(a, n, m) for (int __i = 0; __i < (n); ++__i) for (int __j = 0; __j < m; ++__j) cin >> a[__i][__j];
#define PRINTM(a, n, m) for (int __i = 0; __i < (n); ++__i) { for (int __j = 0; __j < m; ++__j) cout << a[__i][__j] << " ";  cout << endl; };
#define PRINT(a, n) for (int __i = 0; __i < (n); ++__i) cout << a[__i] << " ";
#define IT(a) a.begin(), a.end()
#define CASE(a, s) cout << "Case #" << a << ": " << s << endl;
#define DEB(a) cout << #a << " = " << (a) << endl; cout.flush();
#define DEBA(a) for (auto __i: a) cout << __i << " "; cout << endl;
#define IFDEB(b, a) if (b) { cout << #a << " = " << a << endl; cout.flush(); }
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef vector <int> VI;
typedef vector <vector <int>> VVI;
typedef pair <int, int> PII;
const int MOD = 1000000007;
template <class T> typename T::value_type arr_sum(const T& v, int n) { typename T::value_type sum = 0; FOR(i, 0, n) sum += v[i]; return sum; }
struct Sync_stdio { Sync_stdio() { cin.tie(NULL); ios_base::sync_with_stdio(false); } } _sync_stdio;

template <class T = int> class SegmentTree
{
    int maxn = 150000;
    vector <T> t;
    vector <T> delta;
    vector <int> tl;
    vector <int> tr;
    vector <int> dflag;
    T (*fupd)(T tv, T x, int l, int r);
    T (*fsum)(T x1, T x2);
    T (*dupd)(T dv, T x);
    //void push(int v);
    void build (vector <T> &a, int v, int tleft, int tright)
    {
        tl[v] = tleft;
        tr[v] = tright;
        if (tleft == tright) {
            if (tleft < a.size()) {
                t[v] = a[tleft];
            }
        } else {
            int tmid = (tleft + tright) / 2;
            build (a, v * 2 + 1, tleft, tmid);
            build (a, v * 2 + 2, tmid + 1, tright);
            t[v] = fsum(t[v * 2 + 1], t[v * 2 + 2]);
        }
    }
    void change(int v, LL x)
    {
        t[v] = fupd(t[v], x, tl[v], tr[v]);
        delta[v] = dupd(delta[v], x);
        dflag[v] = 1;
    }
    void update(int v, int l, int r, int x)
    {
        if (l > r) {
            return;
        }
        push(v);
        if (l == tl[v] && tr[v] == r) {
            change(v, x);
        } else {
            int tm = (tl[v] + tr[v]) / 2;
            update (v * 2 + 1, l, min(r, tm), x);
            update (v * 2 + 2, max(l, tm + 1), r, x);
            t[v] = fsum(t[2 * v + 1], t[2 * v + 2]);
        }
    }
    void push(int v)
    {
        if (dflag[v] != 0) {
            if (v * 2 + 1 < 4 * maxn - 1) {
                change(2 * v + 1, delta[v]);
            }
            if (v * 2 + 2 < 4 * maxn - 1) {
                change(2 * v + 2, delta[v]);
            }
            delta[v] = 0;
            dflag[v] = 0;
        }
    }
    LL sum(int v, int l, int r)
    {
        if (l > r) {
            return 0;
        }
        push(v);
        if (l == tl[v] && r == tr[v]) {
            return t[v];
        }
        int tm = (tl[v] + tr[v]) / 2;
        return sum(v * 2 + 1, l, min(r, tm))
+ sum (v * 2 + 2, max(l, tm + 1), r);
    }
    void build(vector <T> &a)
    {
        build(a, 0, 0, maxn - 1);
    }
public:
    int size()
    {
        return maxn;
    }
    SegmentTree(vector <T> &a,
                T (*_fupd)(T tv, T x, int l, int r), //update t in change
                T (*_fsum)(T x1, T x2), //sum
                T (*_dupd)(T dv, T x)) //update delta in change
    {
        maxn = a.size();
        t.resize(4 * maxn);
        delta.resize(4 * maxn);
        tl.resize(4 * maxn);
        tr.resize(4 * maxn);
        dflag.resize(4 * maxn);
        fupd = _fupd;
        fsum = _fsum;
        dupd = _dupd;
        build(a);
    }
    void update(int l, int r, int x)
    {
        update(0, l, r, x);
    }
    LL sum(int l, int r)
    {
        return sum(0, l, r);
    }
    T operator [] (int x)
    {
        return sum(x, x);
    }
};

int main()
{
    int n, m;
    cin >> n >> m;
    vector <LL> a(n);
    FOR (i, 0, n) {
        a[i] = i + 1;
    }
    vector <LL> b(n);
    SegmentTree <LL> t1(a,
    [](LL tv, LL x, int l, int r)
    {
        return x * (r - l + 1);
    },
    [](LL x1, LL x2) {
        return x1 + x2;
    },
    [](LL x1, LL x2) {
        return x2;
    });
    SegmentTree <LL> t2(b,
    [](LL tv, LL x, int l, int r)
    {
        return tv + x * (r - l + 1);
    },
    [](LL x1, LL x2) {
        return x1 + x2;
    },
    [](LL x1, LL x2) {
        return x1 + x2;
    });
    set <int> s;
    FOR (i, 0, n + 1) {
        s.insert(i);
    }
    FOR (i, 0, m) {
        int type;
        cin >> type;
        if (type == 1) {
            int l, r, x;
            cin >> l >> r >> x;
            --l, --r;
            auto p1 = s.upper_bound(l);
            --p1;
            auto p2 = p1;
            ++p2;
            int flag = 0;
            while (*p1 <= r) {
                int tl = max(*p1, l);
                int tr = min(*p2 - 1, r);
                LL y = t1[tl];
                t1.update(tl, tr, x);
                if (flag) {
                    s.erase(p1);
                }
                flag = 1;
                p1 = p2;
                ++p2;
                t2.update(tl, tr, abs(x - y));
            }
            s.insert(l);
            s.insert(r + 1);
        } else {
            /*FOR (i, 0, t1.size()) {
                cout << t1[i] << " ";
            }
            cout << endl;
            FOR (i, 0, t2.size()) {
                cout << t2[i] << " ";
            }
            cout << endl;*/
            int l, r;
            cin >> l >> r;
            --l; --r;
            cout << t2.sum(l, r) << endl;
        }
    }
}
