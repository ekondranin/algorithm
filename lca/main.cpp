#include <iostream>

using namespace std;

vector <vector <int>> up;
int timer = 0;
vector <int> used;
vector <int> tin;
vector <int> tout;
vector <int> par;

void dfs(int t, int p = 0)
{
    used[t] = 1;
    par[t] = p;
    tin[t] = ++timer;
    up[t][0] = p;
    FOR (i, 1, up[t].size()) {
        up[t][i] = up[up[t][i - 1]][i - 1];
    }
    for (auto i: g[t]) {
        if (used[i.first]) {
            continue;
        }
        dfs(i.first, t);
    }
    tout[t] = ++timer;
}


int upper(int a, int b)
{
    return tout[a] >= tout[b] && tin[a] <= tin[b];
}

int lca(int a, int b)
{
    if (upper(a, b)) {
        return a;
    }
    if (upper(b, a)) {
        return b;
    }
    FORR (i, up[a].size() - 1, -1) {
        if (!upper(up[a][i], b)) {
            a = up[a][i];
        }
    }
    return up[a][0];
}

void lca_init()
{
    timer = 0;
    par.resize(g.size());
    used.assign(g.size(), 0);
    tin.resize(g.size());
    tout.resize(g.size());
    up.resize(g.size());
    int l = 1;
    while ((1 << l) <= g.size()) {
        ++l;
    }
    for (auto &i: up) {
        i.resize(l + 1);
    }
    dfs(0);
}

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
