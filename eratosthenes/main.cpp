#include <iostream>

using namespace std;

vector <int> get_primes(int n)
{
    vector <int> prime (n + 1, 1);
    prime[0] = prime[1] = 0;
    int s = sqrt(n);
    for (int i = 2; i <= s; ++i) {
        if (prime[i]) {
            for (int j = i * i; j <= n; j += i) {
                prime[j] = 0;
            }
        }
    }
    vector <int> primes;
    FOR (i, 0, n + 1) {
        if (prime[i]) {
            primes.push_back(i);
        }
    }
    return primes;
}

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
